import './App.css';
import { useState, useEffect } from 'react';
import ItemCard from './components/item-card'

function App() {
  const [items, setItems] = useState([]);
  const [item, setItem] = useState('');

  const apiUrl = "http://localhost:3001";

  useEffect(() => {
    getItems()
  }, [])

  function getItems() {
    fetch(`${apiUrl}/`)
      .then(res => res.json())
      .then(data => {
        setItems(data);
      })
  }

  async function deleteItem (id) {
    await fetch(`${apiUrl}/deleteItem`, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ _id: id}),
    }).then(res => res.text()).then(res => {
      getItems();
      alert(res);
    });
  }

  async function addItem () {
    await fetch(`${apiUrl}/addItem`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ item: item }),
    })
      .then(res => res.text())
      .then(data => {
        setItems([...items, JSON.parse(data)]);
        setItem('');
      }).catch(e => console.error(e));
  }

  return (
    <div className="App">
    <h3>To do List</h3>
      <div key="addItem" className="addItem">
          <input 
            placeholder="Insert task to add to list"
            data-testid="item-input"
            value={item}
            onChange={obj => setItem(obj.target.value)}
            />
          <button onClick={addItem} data-testid="Add">Add</button>
      </div>

      <div className="App-body" data-testid="itemlist">
        {items.length > 0 && items.map((i) => (
          <ItemCard key={i._id} item={i.item} delete={() => deleteItem(i._id)}/>
        ))}
      </div>
    </div>
  );
}

export default App;
