import './itemcard.css';

function ItemCard(props) {
    return (
        <div className="itemcard-wrapper">
            <p className="itemcard-item" data-testid='itemcard-item'>{props.item}</p>
            <button 
                onClick={props.delete} 
                data-testid="itemcard-button"
                className="itemcard-button"
            >
                Delete
            </button>
        </div>
    );
}

export default ItemCard;