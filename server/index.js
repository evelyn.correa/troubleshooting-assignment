var apm = require('elastic-apm-node');
apm.start({
    serviceName: 'troubleshooting-assignment',
    secretToken: 'R2f5GMdP6TCo3uxwz3FYSzR4',
    serverUrl: 'http://localhost:8200',
    environment: 'production'
  })
  

const bodyParser = require('body-parser');
const express = require('express');

const { MongoClient, ObjectId } = require("mongodb");
const { PORT, mongoUri, nameDB, nameColl } = require('../utils/constants');

const app = express();

let database, collection;

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

app.use(bodyParser.json());

app.get("/", async(req, res) => {
    let dblist = [];
    await collection.find({ }).forEach(r => dblist.push(r));
    res.json(dblist);
})

app.post("/addItem", async(req, res) => {
    await collection.insertOne(req.body, (err, result) => {
        if(err) {
            return res.status(500).send(err)
        }
        res.send(result.ops[0]);
    });
})

app.delete("/deleteItem", async(req, res) => {
    await collection.deleteOne({ _id: ObjectId(req.body._id)}, (err, result) => {
        if(err) {
            return res.status(500).send(err);
        }
        res.send(`Item ${req.body._id} deleted successfully!`);
    })
})

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
     MongoClient.connect(mongoUri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(nameDB);
        collection = database.collection(nameColl);
        console.log(`Connected to ${nameDB} and ${nameColl}` );
    });
})
