const constants = {
    mongoUri: "mongodb+srv://app-user:app-password@todo-list.pfzua.mongodb.net/todolist?retryWrites=true&w=majority",
    nameDB: "todo-list",
    nameColl: "todolist",
    
    PORT: 3001,   

    esHost: "https://observability-deployment-ce4ff3.kb.southamerica-east1.gcp.elastic-cloud.com:9243",
}

module.exports = constants;