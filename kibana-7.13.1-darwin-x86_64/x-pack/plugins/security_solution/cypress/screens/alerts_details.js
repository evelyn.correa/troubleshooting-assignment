"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JSON_VIEW_TAB = exports.JSON_LINES = exports.JSON_CONTENT = void 0;
/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License
 * 2.0; you may not use this file except in compliance with the Elastic License
 * 2.0.
 */

const JSON_CONTENT = '[data-test-subj="jsonView"]';
exports.JSON_CONTENT = JSON_CONTENT;
const JSON_LINES = '.ace_line';
exports.JSON_LINES = JSON_LINES;
const JSON_VIEW_TAB = '[data-test-subj="jsonViewTab"]';
exports.JSON_VIEW_TAB = JSON_VIEW_TAB;