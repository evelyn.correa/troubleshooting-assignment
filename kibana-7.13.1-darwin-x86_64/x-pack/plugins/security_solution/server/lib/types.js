"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Configuration", {
  enumerable: true,
  get: function () {
    return _config.ConfigType;
  }
});

var _config = require("../config");