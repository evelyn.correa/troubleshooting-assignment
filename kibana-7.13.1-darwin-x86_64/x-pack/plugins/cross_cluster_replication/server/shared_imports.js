"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "isEsError", {
  enumerable: true,
  get: function () {
    return _server.isEsError;
  }
});
Object.defineProperty(exports, "License", {
  enumerable: true,
  get: function () {
    return _server2.License;
  }
});

var _server = require("../../../../src/plugins/es_ui_shared/server");

var _server2 = require("../../license_api_guard/server");