"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "setupOverviewPage", {
  enumerable: true,
  get: function () {
    return _overview.setup;
  }
});
Object.defineProperty(exports, "OverviewTestBed", {
  enumerable: true,
  get: function () {
    return _overview.OverviewTestBed;
  }
});
Object.defineProperty(exports, "setupIndicesPage", {
  enumerable: true,
  get: function () {
    return _indices.setup;
  }
});
Object.defineProperty(exports, "IndicesTestBed", {
  enumerable: true,
  get: function () {
    return _indices.IndicesTestBed;
  }
});
Object.defineProperty(exports, "setupEnvironment", {
  enumerable: true,
  get: function () {
    return _setup_environment.setupEnvironment;
  }
});

var _overview = require("./overview.helpers");

var _indices = require("./indices.helpers");

var _setup_environment = require("./setup_environment");