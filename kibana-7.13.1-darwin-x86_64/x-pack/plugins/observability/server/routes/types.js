"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ObservabilityServerRouteRepository", {
  enumerable: true,
  get: function () {
    return _get_global_observability_server_route_repository.ObservabilityServerRouteRepository;
  }
});

var _get_global_observability_server_route_repository = require("./get_global_observability_server_route_repository");