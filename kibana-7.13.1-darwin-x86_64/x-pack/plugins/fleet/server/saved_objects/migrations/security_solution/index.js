"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "migratePackagePolicyToV7110", {
  enumerable: true,
  get: function () {
    return _to_v7_11_.migratePackagePolicyToV7110;
  }
});
Object.defineProperty(exports, "migratePackagePolicyToV7120", {
  enumerable: true,
  get: function () {
    return _to_v7_12_.migratePackagePolicyToV7120;
  }
});
Object.defineProperty(exports, "migrateEndpointPackagePolicyToV7130", {
  enumerable: true,
  get: function () {
    return _to_v7_13_.migrateEndpointPackagePolicyToV7130;
  }
});

var _to_v7_11_ = require("./to_v7_11_0");

var _to_v7_12_ = require("./to_v7_12_0");

var _to_v7_13_ = require("./to_v7_13_0");