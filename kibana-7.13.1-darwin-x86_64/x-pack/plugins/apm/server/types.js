"use strict";
/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License
 * 2.0; you may not use this file except in compliance with the Elastic License
 * 2.0.
 */

const requiredDependencies = ['features', 'apmOss', 'data', 'licensing', 'triggersActionsUi', 'embeddable', 'infra', 'observability'];
const optionalDependencies = ['spaces', 'cloud', 'usageCollection', 'taskManager', 'actions', 'alerting', 'security', 'ml', 'home', 'maps'];