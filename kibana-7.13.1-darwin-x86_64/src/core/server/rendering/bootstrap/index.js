"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "registerBootstrapRoute", {
  enumerable: true,
  get: function () {
    return _register_bootstrap_route.registerBootstrapRoute;
  }
});
Object.defineProperty(exports, "bootstrapRendererFactory", {
  enumerable: true,
  get: function () {
    return _bootstrap_renderer.bootstrapRendererFactory;
  }
});

var _register_bootstrap_route = require("./register_bootstrap_route");

var _bootstrap_renderer = require("./bootstrap_renderer");