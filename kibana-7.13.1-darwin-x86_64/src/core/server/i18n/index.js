"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "config", {
  enumerable: true,
  get: function () {
    return _i18n_config.config;
  }
});
Object.defineProperty(exports, "I18nService", {
  enumerable: true,
  get: function () {
    return _i18n_service.I18nService;
  }
});

var _i18n_config = require("./i18n_config");

var _i18n_service = require("./i18n_service");