"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "SavedObjectsClientProvider", {
  enumerable: true,
  get: function () {
    return _scoped_client_provider.SavedObjectsClientProvider;
  }
});
Object.defineProperty(exports, "SavedObjectsErrorHelpers", {
  enumerable: true,
  get: function () {
    return _errors.SavedObjectsErrorHelpers;
  }
});
Object.defineProperty(exports, "SavedObjectsUtils", {
  enumerable: true,
  get: function () {
    return _utils.SavedObjectsUtils;
  }
});

var _scoped_client_provider = require("./scoped_client_provider");

var _errors = require("./errors");

var _utils = require("./utils");