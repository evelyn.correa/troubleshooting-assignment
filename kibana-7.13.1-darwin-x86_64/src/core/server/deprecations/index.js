"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "DeprecationsService", {
  enumerable: true,
  get: function () {
    return _deprecations_service.DeprecationsService;
  }
});

var _deprecations_service = require("./deprecations_service");