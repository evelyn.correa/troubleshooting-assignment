"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "TutorialsRegistry", {
  enumerable: true,
  get: function () {
    return _tutorials.TutorialsRegistry;
  }
});
Object.defineProperty(exports, "TutorialsCategory", {
  enumerable: true,
  get: function () {
    return _tutorials.TutorialsCategory;
  }
});
Object.defineProperty(exports, "SampleDataRegistry", {
  enumerable: true,
  get: function () {
    return _sample_data.SampleDataRegistry;
  }
});

var _tutorials = require("./tutorials");

var _sample_data = require("./sample_data");